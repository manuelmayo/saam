## PROYECTO
[![Generic badge](https://img.shields.io/badge/<DESARROLLO>-<ON>-<COLOR>.svg)](https://shields.io/)
##### Desarrollo de aplicaciones web (DAW)
##### IES San Clemente
##### Santiago de Compostela - A Coruña - España.
---
### Servicio de alojamiento de archivos online
---
### :small_blue_diamond: ANÁLISIS
---
#### UTILIDAD
Compartir archivos de forma muy sencilla e intuitiva entre empleados de una empresa sin depender de aplicaciones externas.
#### TECNOLOGÍAS UTILIZADAS
* __Debian__ como sistema operativo del Servidor.  
* __Servidor HTTP Apache__ como servidor web HTTP.  
* __MySQL__ como sistema gestor de Bases de Datos.  
* __Python__ como lenguaje de programación del lado del servidor.  
* __Django__ como framework de Python para aplicaciones web.  
* __Javascript__ como lenguaje de programación del lado del cliente.  
* __ExtJS__ como framework de Javascript para la creación de aplicaciones web.  
---
### :small_blue_diamond: APP
---
#### ESTRUCTURA DE ARCHIVOS
```bash
saam/  
  |-- manage.py  
  +-- saam/  
        │-- __init__.py  
        │-- settings.py  
        │-- urls.py  
        +-- wsgi.py
```
---
### :small_orange_diamond: AUTOR
Manuel Mayo Lado  
a13manuelml@iessanclemente.net  
manuel.mayo.lado@gmail.com